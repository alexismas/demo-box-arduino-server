var type = "WebGL"
if(!PIXI.utils.isWebGLSupported()){
    type = "canvas"
}

PIXI.utils.sayHello(type)
 // IO
 var socket = io.connect();
 var serialArduino = "0";
 var UpdateColor = 0x00FF00;

 socket.on('project',function(valeur)// appel de la fonction project
{ 
    if(valeur.name =="none" )
    {
        //open(location, '_self').close();
        var win = window.open("about:blank", "_self");
         win.close();
        
    }

});
socket.on('value',function(valeur)// appel de la fonction lecture
{ 
     serialArduino = parseInt(valeur); 
     //console.log(serialArduino);                   
                                
});
// convert RGB to HEX
function rgbToHex(r, g, b) {
    return "0x" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}
function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
//Create the renderer
var app = new PIXI.Application(800, 600, {backgroundColor : 0x1099bb});

//Add the canvas to the HTML document
document.body.appendChild(app.view);

//Create a container object called the `stage`
var eyeL = PIXI.Sprite.fromImage("images/eyes.png");
eyeL.position = new PIXI.Point(415, 300);
var eyeR = PIXI.Sprite.fromImage("images/eyes.png");
eyeR.position = new PIXI.Point(330, 300);
var graphics = new PIXI.Graphics();
graphics.beginFill(0xFF3300);
graphics.lineStyle(4, 0x00FF00, 1);

// draw a shape
graphics.moveTo(400,300);
graphics.lineTo(430,300);
graphics.endFill();

var head = new PIXI.Graphics();
head.beginFill(0x00FF00);
head.lineStyle(4, 0x00FF00, 1);

// draw a shape
head.drawCircle(400, 340, 80);
head.endFill();

app.stage.addChild(head);
app.stage.addChild(graphics);
app.stage.addChild(eyeL);
app.stage.addChild(eyeR);

var videoInput = document.getElementById('inputVideo');
var canvasInput = document.getElementById('inputCanvas');

var htracker = new headtrackr.Tracker();
htracker.init(videoInput, canvasInput);
htracker.start();

document.addEventListener('facetrackingEvent', 
    function (event) {

        
        if(serialArduino< 25)
        {   
            UpdateColor = rgbToHex(parseInt(serialArduino*255/25),255,0);
        }
        else if(serialArduino<=101 )
        {
            UpdateColor = rgbToHex(255,255-(parseInt((serialArduino-25)*254/76)),0);;
        }
       //console.log(255-(parseInt((serialArduino-25)*254/76)))

        //console.log("height: " + event.height + " Width: " + event.width + " angle: " + event.angle + " x: " + event.x + " y: " + event.y);
        eyeL.x = ContrainToRange(event.x+30,    300, 0,     0, 800,     0, 300);
        eyeL.y = ContrainToRange(event.y,       0, 300,     0, 600,     0, 300);
        eyeR.x = ContrainToRange(event.x-30,    300, 0,     0, 800,     0, 300);
        eyeR.y = ContrainToRange(event.y,       0, 300,     0, 600,     0, 300);

        graphics.clear();
        graphics.beginFill(0xFF3300);
        graphics.lineStyle(20, UpdateColor, 1);

        // draw a shape
        graphics.moveTo(420,300);
        graphics.lineTo(eyeR.x+eyeR.width/2,eyeR.y+eyeR.height/2);
        graphics.moveTo(380,300);
        graphics.lineTo(eyeL.x+eyeL.width/2,eyeL.y+eyeL.height/2);
        graphics.endFill();  
        // head
        head.beginFill(UpdateColor);
        head.lineStyle(4, UpdateColor, 1);
        // draw a shape
        head.drawCircle(400, 340, 80);
        head.endFill();

      
    }
);

document.addEventListener('headtrackingEvent', function(event)
{
    //console.log(event.z);
    eyeR.scale.x = eyeR.scale.y = ContrainToRange(event.z, 40, 100, 3, 0.5, 40, 100);
    eyeL.scale.x = eyeL.scale.y = ContrainToRange(event.z, 40, 100, 3, 0.5, 40, 100);
});

function ContrainToRange(value, low1, high1, low2, high2, min, max)
{
    var res = Constrain(value, min, max);
    return MapRange(res, low1, high1, low2, high2);
}

function Constrain(value, min, max)
{
    if(value > max)
        return max;
    else if(value < min)
        return min;
    return value;
}
function MapRange(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}